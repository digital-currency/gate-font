import axios from "axios";

//定义axios请求
export const request = axios.create({
    baseURL: "http://localhost:8080", // 设置基础 URL
    headers: {
        'token': localStorage.getItem('token')
    }
  });