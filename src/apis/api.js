import axios from "axios";
import { request } from './header.js'

//定义axios请求
const axioss = axios.create({
    baseURL: "http://localhost:8080", // 设置基础 URL
  });
//登录功能
export const login = (user) => {
return axioss({
   method: "post",
   url: "/user.do?operate=userLogin",  
   data:user             
})
};
//退出登录功能
export const logout = (user) => {
   return request({
      method: "get",
      url: "/user.do?operate=userLogout",
   })
};
//注册功能
export const register = (user) => {
  return axioss({
     method: "post",
     url: "/user.do?operate=registerUser",  
     data:user             
  })
  };

//获取所有的数字货币信息
export const getAllCurrency = () => {
    return axioss({
        method: "get",
        url: "/currency.do?operate=getCurrency",
    })
};

// 获取通知标题--对应轮播图下面的滚动标题
export const getMessageTitle = () => {
    return axioss({
        method: "get",
        url: "/notification.do?operate=getNotificationTitle",
    })
}

// 获取通知所有信息
export const getMessageAll = () => {
    return axioss({
        method: "get",
        url: "/notification.do?operate=getNotificationList",
    })
}

// 获取单个通知的所有信息
export const getMessageById = (id) => {
    return axioss({
        method: "get",
        url: `/notification.do?operate=getNotification&id=${id}`,
    })
}


// 获取轮播图
export const getSlideshow = () => {
    return axioss({
        method: "get",
        url: "/slideshow.do?operate=getSlideshowList",
    })
}

// 根据id获取轮播图
export const getSlideshowById = (id) => {
    return axioss({
        method: "get",
        url: `/slideshow.do?operate=getSlideshowById&id=${id}`,
    })
}

//获取所有订单
export const getAllOrders = () => {
   return axioss({
      method: "get",
      url: "/order.do?operate=getAllOdersList",
   })
};
//获取自己所有订单
export const getAllMyOrders = () => {
   //console.log(localStorage.getItem("token"));
   return request({
      method: "get",
      url: "/order.do?operate=getOdersList",
      headers: {
         'token': localStorage.getItem("token")
      }
   })
};
//输入金额返回数字货币数量
export const getNumbers = (price,id) => {
   return request({
      method: "get",
      url: "/currency.do?operate=getCurrencyNumber",
     /* headers: {
         'token': localStorage.getItem("token")
      },*/
      params: {
         "bigDecimal": price, // 输入框输入金额
         "id": id // Long 法币ID
      }
   })
};
//卖出数字货币
export const sellCurrency = (data) => {
   return request({
      method: "post",
      url: "/currency.do?operate=sellCurrency",
      headers: {
         'token': localStorage.getItem("token")
      },
      data:data
   })
};
//买入数字货币
export const buyCurrency = (data) => {
   return request({
      method: "post",
      url: "/currency.do?operate=buyCurrency",
      headers: {
         'token': localStorage.getItem("token")
      },
      data:data
   })
};
//获取用户信息功能
export const getUserMessage = () => {
   return axioss({
      method: "get",
      url: "/user.do?operate=getCurrentUser",
      headers: {
         'token': localStorage.getItem("token")
      },
   })
}
//获取当前⽤户的数字货币信息
export const getUserCurrency = () => {
   return axioss({
      method: "get",
      url: "/currency.do?operate=getUserCurrency",
      headers: {
         'token': localStorage.getItem("token")
      },
   })
}
//充值接口
export const charge = () => {
   return axioss({
      method: "post",
      url: "/user.do?operate=addBalance",
      headers: {
         'token': localStorage.getItem("token")
      },
      params:{
         'money': 10000
      }
   })
}

// 获取利率
export const wealthManagement = (data) => {
    return request({
        method: "post",
        url: "/wealthManagement.do?operate=save",
        data:data
    })
}

//获取StartUp首发的货币信息
export const startUpPublish = () => {
    return axioss({
        method: "get",
        url: "/firstPublish.do?operate=getFirstPublishList",
    })
};


