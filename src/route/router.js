import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('../views/Home.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/bibi',
        component: () => import('../views/Coin.vue'),
        meta: {
            status: 1
        }
    },
    {
        path: '/Heyue',
        component: () => import('../views/Heyue.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/Wallet',
        component: () => import('../views/Wallet.vue'),
        meta: {
            status: 1
        }
    },
    {
        path: '/Account',
        component: () => import('../views/Account.vue'),
        meta: {
            status: 1
        }
    },
    {
        path: '/biquan',
        component: () => import('../views/Biquan/Biquan.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/login',
        component: () => import('../views/Login.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/register',
        component: () => import('../views/Register.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/fabi',
        component: () => import('../views/Home/menu-transaction.vue'),
        meta: {
            status: 1
        }
    },
    {
        path: '/licaibao',
        component: () => import('../views/Home/menu-money.vue'),
        meta: {
            status: 1
        }
    },
    {
        path: '/startup',
        component: () => import('../views/Home/menu-startup.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/message/:id',
        component: () => import('../views/message/message.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/messageAll',
        component: () => import('../views/message/messageAll.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/publish',
        component: () => import('../views/Biquan/Publish.vue'),
        meta: {
            status: 0
        }
    },
    {
        path: '/banner/:id',
        component: () => import('../views/banner.vue'),
        meta: {
            status: 0
        }
    },
]
const router = createRouter({
    routes,
    history: createWebHistory()
});

// ... 上面的导入和路由配置 ...

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token');

    if (to.path == '/login' && token) {
        //登录了不能进入登录页面
        next('/');
    }
    else if (to.meta.status == 1 && !token) {
        // 如果需要认证但用户未登录，则重定向到登录页面
        next('/login');
    } else {
        // 否则继续正常的导航
        next();
    }
});

export default router