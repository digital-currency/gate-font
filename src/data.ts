import { ref} from "vue";


// 轮播图下面的新闻
// 后端传数据
export const msgList = [
    {
        id: 1001,
        title: "Gate.io已上線(WWW)永續合約交易(USDT結算),速来...",
        time: "2023/10/7 15:30:08",
        count: 3346,
        path: 0,
        content:
            "Gate.io永續合約是全球最活躍的區塊鏈資產合約市場之一，日交易量高達8億美金，擁有超過1000ETH的保險...",
    },
    {
        id: 1002,
        title: "以太坊最新会议:Devnet-9当前网参与率接近90%,NB...",
        time: "2023/10/7 22:38:08",
        count: 1008,
        path: 0,
        content:
            "PANews 10月7日消息，Galaxy研究副总裁Christine Kim发文总结第119次以太坊核心开发者共识会议（ACD...",
    },
    {
        id: 1003,
        title: "Tip组织最新会议:发现Epoch 2申领相关问题,完犊子...",
        time: "2023/10/7 15:30:08",
        count: 1346,
        path: 0,
        content:
            "10月7日消息，Web3社交应用Tip Coin于5:00左右开启Epoch 2阶段申领，随后发文表示，发现申领相关问题...",
    },
    {
        id: 1004,
        title: "Caroline Ellison或将于下周二SBF审判中出庭作证,恐怖",
        time: "2023/10/7 07:30:00",
        count: 10086,
        path: 0,
        content:
            "PANews 10月7日消息，据CoinDesk报道，对冲基金Alameda Research前首席执行官Caroline Ellison很可...",
    },
    {
        id: 1005,
        title: "英国因青少年隐私问题对Snap AI聊天机器人展开调查",
        time: "2023/10/7 05:10:18",
        count: 14651,
        path: 0,
        content:
            "据站长之家 10 月 7 日报道，Snap 公司因其生成式人工智能聊天机器人可能对 Snapchat 用户，尤其是 13 至 ...",
    },
];

// 中间菜单栏
export const menuList = [
    { id: 1, url: "p1", name: "法币交易" ,path:'/fabi' },
    { id: 2, url: "p2", name: "理财宝",path:'/licaibao' },
    { id: 3, url: "p3", name: "StartUp首发" ,path:'/startup'},
     { id: 4, url: "p4", name: "MiniApp" },
     { id: 5, url: "p5", name: "余额宝" },
     { id: 6, url: "p6", name: "财富管理" },
     { id: 7, url: "p7", name: "量化限单" },
     { id: 8, url: "p8", name: "大数据" },
     { id: 9, url: "p9", name: "NFT" },
     { id: 10, url: "p10", name: "更多" },
];

export const tagList = [
    { id: 1, title: "涨幅榜" },
    { id: 2, title: "现货" },
    { id: 3, title: "合约" },
    { id: 4, title: "ETF" },
    { id: 5, title: "借贷" },
    { id: 6, title: "新币" },
];

// 数据等后端传过来
export const dataList = [
    {
        id: 1001,
        name: "BCT",
        usdtPrice: 69999,
        rmbPrice: 500000,
        total: 300,
        priceIncrease: "+10.86",
        icon: "assets/dc/btc.png",
    },
    {
        id: 1002,
        name: "ETH",
        usdtPrice: 4400,
        rmbPrice: 30000,
        total: 168,
        priceIncrease: "+12.36",
        icon: "assets/dc/eth.png",
    },
    {
        id: 1003,
        name: "GT",
        usdtPrice: 4.4,
        rmbPrice: 33,
        total: 12.5,
        priceIncrease: "+19.58",
        icon: "assets/dc/gt.png",
    },
    {
        id: 1004,
        name: "XRP",
        usdtPrice: 0.44,
        rmbPrice: 3.33,
        total: 68.6,
        priceIncrease: "+33.22",
        icon: "assets/dc/xrp.png",
    },
    {
        id: 1005,
        name: "STARL",
        usdtPrice: 0.00044,
        rmbPrice: 0.0033,
        total: 6.68,
        priceIncrease: "+88.88",
        icon: "assets/dc/starl.png",
    },
    {
        id: 1006,
        name: "LOOM",
        usdtPrice: 1.2,
        rmbPrice: 9.33,
        total: 0.68,
        priceIncrease: "-10.88",
        icon: "assets/dc/loom.png",
    },
    {
        id: 1007,
        name: "ASTRA",
        usdtPrice: 2.2,
        rmbPrice: 15.33,
        total: 0.98,
        priceIncrease: "-30.88",
        icon: "assets/dc/astra.png",
    },
    {
        id: 1008,
        name: "WMS",
        usdtPrice: 0.25,
        rmbPrice: 1.75,
        total: 3.68,
        priceIncrease: "-36.88",
        icon: "assets/dc/wsm.png",
    },
    {
        id: 1009,
        name: "AMS",
        usdtPrice: 0.0025,
        rmbPrice: 0.0175,
        total: 0.98,
        priceIncrease: "-16.88",
        icon: "assets/dc/ams.png",
    },
    {
        id: 1010,
        name: "TOMI",
        usdtPrice: 8.25,
        rmbPrice: 55.175,
        total: 10.98,
        priceIncrease: "-06.88",
        icon: "assets/dc/tomi.png",
    },
    {
        id: 1011,
        name: "VRA",
        usdtPrice: 3.2,
        rmbPrice: 22.14,
        total: 0.98,
        priceIncrease: "+66.88",
        icon: "assets/dc/vra.png",
    },
    {
        id: 1012,
        name: "FLOKI",
        usdtPrice: 0.0000032,
        rmbPrice: 0.00002214,
        total: 11.55,
        priceIncrease: "-99.88",
        icon: "assets/dc/floki.png",
    },
];

// 币圈中的动态内容
export const dynamicItems = ref([
    {
        avatar: 'src/assets/dc/btc.png',
        nickname: '马斯克的小迷弟',
        time: '18分钟前',
        content: '一个投机者必备的素质是:从来都不和大对，拥有无畏的勇气，但并不鲁莽，一旦发现自己做错，瞬间回头，立刻改正。',
        img: 'src/assets/contract/06.png',
    },
    {
        avatar: 'src/assets/dc/btc.png',
        nickname: '昵称已被占用',
        time: '8分钟前',
        content: '一个投机者必备的素质是:从来都不和大盘做对，拥有无畏的勇气，但并不鲁莽，一旦发现自己做错，瞬间回头，立刻改正。',
        img: 'src/assets/contract/06.png',
    },
    {
        avatar: 'src/assets/dc/btc.png',
        nickname: '昵称已被占用',
        time: '8分钟前',
        content: '一个投机者必备的素质是:从来都不和大盘做对，拥有无畏的勇气，但并不鲁莽，一旦发现自己做错，瞬间回头，立刻改正。',
        img: 'src/assets/contract/06.png',
    }
]);