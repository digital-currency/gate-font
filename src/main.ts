import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "./style.css";
import App from "./App.vue";

const app = createApp(App);
//使用ElementUI组件
app.use(ElementPlus);
//使用路由
import router from "./route/router.js";
app.use(router);
//使用axios发送请求
import axios from "axios"
app.use(axios)

//挂载应用
app.mount("#app");