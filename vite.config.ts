import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  devServer: {
    // 设置开发环境代理
    proxy: {
      '/apis': {
        target: 'https://localhost:8080', // 目标 API 服务器
        changeOrigin: true,
        pathRewrite: { '^/apis': '' }, // 去掉请求中的 /api
      },
    },
  },
});